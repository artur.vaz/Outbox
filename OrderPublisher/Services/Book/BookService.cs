﻿using OrderConsumer.Context;
using OrderPublisher.Dto;
using OrderPublisher.Repository.Book;
using Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using System.Transactions;

namespace OrderPublisher.Services.Book
{
    public class BookService : IBookService
    {
        private readonly IBookRepository _bookRepository;
        private readonly RabbitMqContext _context;

        public BookService(IBookRepository bookRepository, RabbitMqContext context)
        {
            _bookRepository = bookRepository;
            _context = context;
        }
        public async Task<BookResponseDto> Create(BookRequestDto bookRequest)
        {                        
            var book = _bookRepository.Create(new BookModel
            {
                Description = bookRequest.Description,
                Title = bookRequest.Title,
                Isbn = bookRequest.Isbn,
                Year = bookRequest.Year,
                BookGuid = Guid.NewGuid().ToString()
            });

            var queue = new InfOutboxMessageModel
            {
                Exchange = "book.exchange",
                RoutingKey = "book.routingKey",
                Redelivered = "book.redelevered",
                Properties = "book.properties",
                Payload = JsonSerializer.Serialize(book),
                QueueName = "book.Artur.queue",
                CreatedAt = DateTime.Now,
                Available = true
            };

            _context.Queues.Add(queue);
            await _context.SaveChangesAsync();
            
            return new BookResponseDto
            {
                Id = book.Id,
                Description = book.Description,
                FlPublicDomain = book.FlPublicDomain,
                Isbn = book.Isbn,
                Title = book.Title,
                Year = book.Year,
                BookGuid = book.BookGuid.ToString()
            };
        }

        public Task<bool> UpdatePublicDomainWithOver70years()
        {
            throw new NotImplementedException();
        }
    }

}
