﻿using OrderPublisher.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderPublisher.Services.Book
{
    public interface IBookService
    {
        Task<BookResponseDto> Create(BookRequestDto bookRequest);
        Task<bool> UpdatePublicDomainWithOver70years();
    }
}
