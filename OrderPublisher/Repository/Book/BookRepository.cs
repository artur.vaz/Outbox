﻿using OrderConsumer.Context;
using Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderPublisher.Repository.Book
{
    public class BookRepository : IBookRepository
    {
        private readonly RabbitMqContext _context;
        public BookRepository(RabbitMqContext context)
        {
            _context = context;
        }
        public BookModel Create(BookModel bookO)
        {
            _context.Books.Add(bookO);
            _context.SaveChanges();
            return bookO;
        }
    }
}
