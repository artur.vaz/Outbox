﻿using Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderPublisher.Repository.Book
{
    public interface IBookRepository
    {
        BookModel Create(BookModel bookO);
    }
}
