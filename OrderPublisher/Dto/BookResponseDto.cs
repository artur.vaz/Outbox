﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderPublisher.Dto
{
    public record BookResponseDto
    {
        public int Id { get; init; }
        public string Title { get; init; }
        public string Description { get; init; }
        public string Isbn { get; init; }
        public bool FlPublicDomain { get; init; }
        public int Year { get; init; }
        public string BookGuid { get; set; }
    }

}
