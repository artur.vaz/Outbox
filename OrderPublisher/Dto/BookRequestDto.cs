﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OrderPublisher.Dto
{
    public record BookRequestDto
    {
        [Required(ErrorMessage = "Erro de validação: A titulo do livro é obrigatória", AllowEmptyStrings = false)]
        [MaxLength(100)]
        [MinLength(3)]
        public string Title { get; set; }

        [Required(ErrorMessage = "Erro de validação: A descrição do livro é obrigatória", AllowEmptyStrings = false)]
        [MaxLength(250)]
        [MinLength(3)]
        public string Description { get; set; }

        [Required(ErrorMessage = "Erro de validação: Isbn é obrigatório", AllowEmptyStrings = false)]
        [MaxLength(100)]
        [MinLength(3)]
        public string Isbn { get; set; }
        public int Year { get; set; }        
    }


}
