﻿using MassTransit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OrderConsumer.Context;
using Shared.Model;
using System;
using System.Text.Json;
using System.Threading.Tasks;

namespace OrderPublisher.Controllers
{
    [Route("api/[controller]")]
    [ApiController]    
    public class OrderController : ControllerBase
    {
        private readonly IBus _bus;
        private readonly ILogger<OrderController> _logger;
        private readonly RabbitMqContext _context;
        public OrderController(IBus bus, ILogger<OrderController> logger)
        {
            _bus = bus;
            _logger = logger;
        }
        #region documentação
        ///// <summary>
        ///// Creates a TodoItem.
        ///// </summary>
        ///// <param name="item"></param>
        ///// <returns>A newly created TodoItem</returns>
        ///// <remarks>
        ///// Sample request:
        /////
        /////     POST /Todo
        /////     {        
        /////        "userName": 1,
        /////        "booked": "2023-08-30",
        /////        "location": true
        /////     }
        /////
        ///// </remarks>
        ///// <response code="201">Returns the newly created item</response>
        ///// <response code="400">If the item is null</response>        
        //[ProducesResponseType(StatusCodes.Status201Created)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        #endregion
        [HttpPost]
        [Produces("application/json")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> CreateTicket([FromBody] Ticket ticket)
        {
            if (ticket != null)
            {
                ticket.Booked = DateTime.Now;
                Uri uri = new Uri("rabbitmq://localhost/orderTicketQueue");
                var endPoint = await _bus.GetSendEndpoint(uri);
                //_logger.LogInformation($"Send {nameof(ticket)}");
                await endPoint.Send(ticket);
                return Ok();
            }
            return BadRequest();
        }

        [HttpPost]
        [Route("CreateTarefa")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> CreateTarefa(TarefaModel tarefa)
        {            
            if (tarefa != null)
            {                
                Uri uri = new("rabbitmq://localhost/tarefaQueue");
                var endPoint = await _bus.GetSendEndpoint(uri);
                int i = 0;
                var novaTarefa = tarefa.Tarefa;
                while (i <= 10)
                {                    
                    tarefa.Tarefa = $"{novaTarefa} : {i}";
                    await endPoint.Send(tarefa);
                    i++;
                }
                var options = new JsonSerializerOptions { WriteIndented = true };
                string jsonString = JsonSerializer.Serialize<TarefaModel>(tarefa, options);
                //_logger.LogInformation($"Em: {DateTime.Now} Nova tarefa enviada para fila: {jsonString}");
                Console.WriteLine($" [x] Sent {DateTime.Now} Nova tarefa enviada para fila: {jsonString}");
                return Ok(tarefa);                                
            }
            return BadRequest();
        }        
    }
}
