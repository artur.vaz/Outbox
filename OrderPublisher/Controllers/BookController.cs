﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OrderPublisher.Dto;
using OrderPublisher.Services.Book;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderPublisher.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly ILogger<BookController> _logger;
        private readonly IBookService _bookService;
        public BookController(ILogger<BookController> logger, IBookService bookService)
        {
            _logger = logger;
            _bookService = bookService;
        }

        [HttpPost("create")]
        public async Task<BookResponseDto> Create(BookRequestDto bookRequest)
        {            
            return await _bookService.Create(bookRequest);            
        }
    }


}
