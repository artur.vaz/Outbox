﻿using Microsoft.EntityFrameworkCore;
using Shared.Model;

namespace OrderConsumer.Context
{
    public class RabbitMqContext : DbContext
    {
        public RabbitMqContext(DbContextOptions<RabbitMqContext> options) : base(options)
        {

        }        
        public DbSet<TarefaModel> Tarefa {get;set;}
        public DbSet<BookModel> Books { get; set; }
        public DbSet<InfOutboxMessageModel> Queues { get; set; }
    }
}
