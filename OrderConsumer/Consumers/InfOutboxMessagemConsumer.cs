﻿using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OrderConsumer.Context;
using Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace OrderConsumer.Consumers
{
    public class InfOutboxMessagemConsumer : IConsumer<BookModel>
    {
        private readonly ILogger<InfOutboxMessageModel> _logger;
        private readonly RabbitMqContext _context;
        public InfOutboxMessagemConsumer(ILogger<InfOutboxMessageModel> logger, RabbitMqContext context)
        {
            this._logger = logger;
            this._context = context;
        }

        public async Task Consume(ConsumeContext<BookModel> bookContext)
        {
            try
            {
                Console.Out.WriteLine($" [x] Received novo livro recebido : {bookContext.Message.Id}");

                var bookUpdate = await _context.Books.Where(b => b.Id == bookContext.Message.Id).FirstOrDefaultAsync();
                if (bookUpdate != null)
                {
                    bookUpdate.AtualizarDominioPublicoComMaisDe70anos();
                    _context.Entry(bookUpdate).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                }
            }catch(Exception ex)
            {
                string aux = ex.Message;
            }
        }        
    }
}