﻿using MassTransit;
using Microsoft.Extensions.Logging;
using Shared.Model;
using System;
using System.Text.Json;
using System.Threading.Tasks;

namespace OrderConsumer.Consumers
{
    public class TicketConsumer : IConsumer<Ticket>
    {
        private readonly ILogger<TicketConsumer> logger;
        public TicketConsumer(ILogger<TicketConsumer> logger)
        {
            this.logger = logger;
        }

        public async Task Consume(ConsumeContext<Ticket> context)
        {
            var options = new JsonSerializerOptions { WriteIndented = true };
            string jsonString = JsonSerializer.Serialize<Ticket>(context.Message, options);
            
            await Console.Out.WriteLineAsync(context.Message.UserName);

            logger.LogInformation($"Nova mensagem recebida : {jsonString}");

            //throw new Exception("erro!");
        }
    }
}
