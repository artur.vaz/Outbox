﻿using MassTransit;
using Microsoft.Extensions.Logging;
using OrderConsumer.Context;
using Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace OrderConsumer.Consumers
{
    public class TarefaConsumer : IConsumer<TarefaModel>
    {
        private readonly ILogger<TarefaConsumer> _logger;
        private readonly RabbitMqContext _context;
        public TarefaConsumer(ILogger<TarefaConsumer> logger, RabbitMqContext contextRM)
        {
            this._logger = logger;
            this._context = contextRM;
        }

        public async Task Consume(ConsumeContext<TarefaModel> tarefaO)
        {
            //await Console.Out.WriteLineAsync($" [x] Received Nova tarefa recebida : {jsonString}");

            var queue = new Queue<TarefaModel>();
            queue.Enqueue(tarefaO.Message);

            if (tarefaO.DestinationAddress.Equals("tarefaQueue_error"))
            {
                string aux = "";
            }
            _logger.LogInformation($"[x] Received Nova tarefa recebida : {MensagemJson(tarefaO.Message)}");

            throw new Exception("Não enviado");
            //try
            //{
                //await _context.Tarefa.AddAsync(tarefa.Message);                
                //await _context.SaveChangesAsync();                
            //}
            //catch (Exception ex)
            //{                
              //  _logger.LogError($"Erro ao tentar adicionar nova tarefa : {jsonString}, Erro: {ex.Message}");                
            //}
        }
        public string MensagemJson(TarefaModel item)
        {
            var options = new JsonSerializerOptions { WriteIndented = true };
            return JsonSerializer.Serialize<TarefaModel>(item, options);
        }
    }
}
