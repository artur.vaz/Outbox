﻿using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OrderConsumer.Context;
using OrderConsumer.Repository;
using Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OrderConsumer
{
    
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;      
        private readonly IServiceProvider _provider;
        private readonly IBus _bus;
        //private readonly IOutboxMessagemRepository _outboxRepository;
        /// <summary>
        /// URI: rabbitmq://172.30.1.45:5672/bookArturQueue
        /// </summary>
        private readonly string EnderecoRabbitMq = new(@"rabbitmq://172.30.1.45:5672/");

        public Worker(ILogger<Worker> logger, IServiceProvider provider, IBus bus)
        {
            _logger = logger;
            _provider = provider;
            _bus = bus;            
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            using var newProvider = _provider.CreateScope();
            var context = newProvider.ServiceProvider.GetRequiredService<RabbitMqContext>();

            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                await Task.Delay(10000, stoppingToken);

                var queues = context.Queues
                    .Where(x => x.Available == true)
                    .ToList()
                    .Take(10);

                //var queues = await _outboxRepository.GetAllNotsynchronizedTo10();
                foreach (var item in queues)
                {
                    var uri = new Uri($"{EnderecoRabbitMq}{item.QueueName}");
                    _logger.LogInformation($"Item para add na fila: {item.Payload} { DateTimeOffset.Now}");

                    var endPoint = await _bus.GetSendEndpoint(uri);
                    var bookO = JsonConvert.DeserializeObject<BookModel>(item.Payload);
                    await endPoint.Send(bookO, stoppingToken);

                    //var bookUpdate = await _outboxRepository.FindById(item.Id);
                    var bookUpdate = await context.Queues.Where(f => f.Id == item.Id).FirstOrDefaultAsync();
                    if (bookUpdate != null)
                    {
                        bookUpdate.Available = false;
                        bookUpdate.UpdatedAt = DateTime.Now;

                        context.Entry(item).State = EntityState.Modified;
                        await context.SaveChangesAsync();
                        //if (await _outboxRepository.UpdateAvailable(bookUpdate))
                        //{
                        //atualizado
                        //}
                    }                            
                }
            }
        }        
    }    
}
