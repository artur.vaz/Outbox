using GreenPipes;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using OrderConsumer.Consumers;
using OrderConsumer.Context;
using OrderConsumer.Repository;
using System;

namespace OrderConsumer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<RabbitMqContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("Outbox")));
            services.AddMassTransit(x =>
            {
                //x.AddConsumer<TicketConsumer>();
                //x.AddConsumer<TarefaConsumer>();
                x.AddConsumer<InfOutboxMessagemConsumer>();

                x.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    cfg.UseHealthCheck(provider);
                    cfg.Host(new Uri("rabbitmq://172.30.1.45:5672/"), h =>
                    {
                        h.Username("root");
                        h.Password("root");
                    });
                    
                    //cfg.ReceiveEndpoint("orderTicketQueue", ep =>
                    //{
                    //    ep.PrefetchCount = 10;
                    //    ep.UseMessageRetry(r => r.Interval(2, 100));
                    //    ep.ConfigureConsumer<TicketConsumer>(provider);
                    //});

                    //cfg.ReceiveEndpoint("tarefaQueue", ep =>
                    //{
                    //    ep.PrefetchCount = 10;                        
                    //    ep.UseMessageRetry(r => r.Interval(2, 100));                        
                    //    ep.ConfigureConsumer<TarefaConsumer>(provider);                        
                    //});
                    cfg.ReceiveEndpoint("book.Artur.queue", ep =>
                    {
                        ep.PrefetchCount = 10;
                        ep.UseMessageRetry(r => r.Interval(2, 100));
                        ep.ConfigureConsumer<InfOutboxMessagemConsumer>(provider);
                    });
                }));
            });
            services.AddMassTransitHostedService();
            services.AddControllers();
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Sigap consumer",
                    Description = "An ASP.NET Core Web API to manage pending weighing",                    
                    Contact = new OpenApiContact
                    {
                        Name = "Artur Vaz",                      
                        Email = "artur.vaz@veolia.com"
                    },                   
                });         
            });
            services.AddHostedService<Worker>();
            services.AddScoped<IOutboxMessagemRepository, OutboxMessagemRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {                
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "OrderConsumer v1"));
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
