﻿using MassTransit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OrderConsumer.Context;
using Shared.Model;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OrderConsumer.Repository
{
    [Route("api/[controller]")]
    [ApiController]    
    public class OrderController : ControllerBase
    {
        private readonly ILogger<OrderController> _logger;
        private readonly RabbitMqContext _context;

        public OrderController(IBus bus, ILogger<OrderController> logger, RabbitMqContext context)
        {
            _logger = logger;
            _context = context;
        }
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> List()
        {            
            return Ok(_context.Books.ToList());
        }
    }
}
