﻿using Microsoft.EntityFrameworkCore;
using OrderConsumer.Context;
using Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderConsumer.Repository
{
    public class OutboxMessagemRepository : IOutboxMessagemRepository
    {        
        private readonly RabbitMqContext _context;
        public OutboxMessagemRepository(RabbitMqContext context)
        {        
            _context = context;
        }
        public async Task<InfOutboxMessageModel> FindById(int id)
        {
            return await _context.Queues.Where(f => f.Id == id).FirstOrDefaultAsync();
        }
        public async Task<IEnumerable<InfOutboxMessageModel>> GetAllNotsynchronizedTo10()
        {
            return await _context.Queues
                   .Where(x => x.Available == true)
                   .Take(10)
                   .ToListAsync();
        }
        public async Task<bool> UpdateAvailable(InfOutboxMessageModel item)
        {
            _context.Entry(item).State = EntityState.Modified;
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
