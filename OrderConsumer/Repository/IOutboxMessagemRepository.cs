﻿using Shared.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrderConsumer.Repository
{
    public interface IOutboxMessagemRepository
    {
        Task<InfOutboxMessageModel> FindById(int id);
        Task<bool> UpdateAvailable(InfOutboxMessageModel item);
        Task<IEnumerable<InfOutboxMessageModel>> GetAllNotsynchronizedTo10();
    }
}
