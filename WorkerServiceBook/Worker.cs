using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using OrderConsumer.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace WorkerServiceBook
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly RabbitMqContext _context;
        public Worker(ILogger<Worker> logger, RabbitMqContext context)
        {
            _logger = logger;
            _context = context;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                await Task.Delay(30000, stoppingToken);

                var queues = _context.Queues
                    .Where(x => x.Available == true);

                foreach (var item in queues)
                {
                    _logger.LogInformation($"Item para add na fila: {item.Payload} { DateTimeOffset.Now}");
                }
            }
        }
    }
}
