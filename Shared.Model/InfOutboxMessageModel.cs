﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Model
{
    [Table("tb_InfOutboxMessage")]
    public class InfOutboxMessageModel
    {
        public int Id { get; set; }
        public string Exchange { get; set; }
        public string RoutingKey { get; set; }
        public string Redelivered { get; set; }
        public string Properties { get; set; }
        public string Payload { get; set; }
        public string QueueName { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Available { get; set; }
    }
}
