﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Model
{
    [Table("tb_book")]

    public class BookModel
    {                
        /// <summary>
        /// Identificador unido do livro
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Titulo do livro
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// O ISBN International Standard Book Number
        /// </summary>
        public string Description { get; set; }
        public string Isbn { get; set; }
        [DefaultValue(false)]
        public bool FlPublicDomain { get; private set; }
        /// <summary>
        /// Ano do Livro
        /// </summary>
        public int Year { get; set; }
        public string BookGuid { get; set; }         
        public BookModel() 
        {
            
        }
        //void IDBookGuid()
        //{
        //    this.BookGuid = Guid.NewGuid();
        //}
        public void AtualizarDominioPublicoComMaisDe70anos()
        {
            FlPublicDomain = DateTime.Now.Year - Year > 70;
            //Console.WriteLine($"Ano atual - 70 é > 70. {DateTime.Now.Year}-{70} > 70");
        }               
    }
}
