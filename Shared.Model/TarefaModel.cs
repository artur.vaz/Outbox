﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Shared.Model
{
    [Table("tb_tarefa")]
    public class TarefaModel
    {                
        [Key]
        [JsonIgnore]
        public long Id { get; set; }
        [Required(ErrorMessage = "O nome da tarefa é obrigatório", AllowEmptyStrings = false)]
        [StringLength (200)]
        [Display(Name = "Nome da tarefa")]
        public string Tarefa { get; set; }
        [JsonIgnore]
        public DateTime? DataCriacao { get; set; } = DateTime.Now;
        [Required(ErrorMessage = "O nome do responsavel é obrigatório", AllowEmptyStrings = false)]
        [StringLength(150)]
        public string Responsavel { get; set; }
    }
}
